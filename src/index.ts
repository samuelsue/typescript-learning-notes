// import './examples/basic-type'  // 1. 基本类型
// import './examples/symbol' // 2. symbol
// import './examples/interface'  // 3. interface
// import './examples/function'  // 4. 函数
// import './examples/generics'  // 5. 泛型
// import './examples/classTest'  // 6. TS中的类
// import './examples/enumTest'  // 7. 枚举
// import './examples/inference-compatibility' // 8. 类型兼容性
// import './examples/advanced-type-1'  // 9. 高级类型1
// import './examples/advanced-type-2' // 10. 高级类型2
// import './ts-modules/index'  // 11. ts模块
// import './examples/merging'  // 12. 声明合并
// import './examples/decorators' // 13. 装饰器

import './examples/typescript-exercise14' // ts-exercise 14题

// import './examples/destructuring' // 解构赋值
// import './examples/reflect-metadata'
