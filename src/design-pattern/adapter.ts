interface Target {
  request():void
}

// 适配者
class Adaptee {
  public specificRequest ():void {
    console.log('specificRequest of Adaptee is being called')
  }
}

class Adapter implements Target {
  public request (): void {
    console.log('Adapter\'s request method is called')
    const adaptee:Adaptee = new Adaptee()
    adaptee.specificRequest()
  }
}

function show ():void {
  const adapter:Adapter = new Adapter()
  adapter.request()
}

// 以下是实例代码， 日志系统
interface Logger {
  info(message:string) :Promise<void>;
}

class FileLogger implements Logger {
  public async info (message: string): Promise<void> {
    console.info(message) // 模拟写入日志
    console.info('This message was saved with FileLogger')
  }
}

class NotificationService {
  protected logger:Logger;
  constructor (logger:Logger) {
    this.logger = logger
  }

  public async send (message:string):Promise<void> {
    await this.logger.info(`Notification sended: ${message}`)
  }
}

(async () => {
  const fileLogger = new FileLogger()
  const notificationService = new NotificationService(fileLogger)
  await notificationService.send('Hello Semlinker, To File')
})()
