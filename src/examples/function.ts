// 类型别名定义函数类型
type Add = (x: number, y: number) => number
const addFunc: Add = (x: number, y: number) => x + y

// 函数可选参数，默认参数
type AddFnType = (x: number, y?: number) => number
const fn2: AddFnType = (x = 3, y) => x + (y || 0)

// 剩余参数
type SumFnType = (...args: number[]) => number
const fn3: SumFnType = (...args) => args.reduce((previousValue, currentValue) => previousValue + currentValue, 0)
console.log(fn3(1, 2, 3, 4, 5, 6))

// 函数重载(ts的重载只是用于代码提示,不是真正意义上的重载)
function splitChar(x: number): string[]
function splitChar(x: string): string[]
function splitChar (x: any): any { // 前两个是方法声明(支持重载类型), 这里才是方法实体(必须这么写)
  if (typeof x === 'string') {
    return x.split('')
  } else {
    return (x as number).toString().split('')
  }
}

console.log(splitChar(123456))
console.log(splitChar('我是爸爸'))
