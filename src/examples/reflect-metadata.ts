import 'reflect-metadata'

function Role (name:string):ClassDecorator {
  // 类装饰器， target是构造函数
  return target => {
    Reflect.defineMetadata('role', name, target.prototype)
    console.log(target)
  }
}

@Role('admin')
class Post {}

const metaData = Reflect.getMetadata('role', Post.prototype)
console.log(metaData)
const np = new Post()
const anotherMd = Reflect.getMetadata('role', (np as any).__proto__)
console.log(anotherMd)
