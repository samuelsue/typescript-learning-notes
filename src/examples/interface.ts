// 使用接口限制属性类型
interface NameInterface {
  firstName: string,
  lastName: string
}

// 限制对象作为参数传递时属性的类型
function getFullname ({ firstName, lastName }: NameInterface): string {
  return `${firstName}·${lastName}`
}

console.log(getFullname({
  firstName: 'Samuel',
  lastName: 'Sue'
}))

// 对于可选参数接口
interface VegetableInterface {
  color?: string, // 可以有color也可以没有
  type: string
}

function VegInfo ({ color, type }: VegetableInterface): string {
  return `${color || ''}${type}`
}

console.log(VegInfo({ type: 'Onion' }))

enum Gender {
  Female,
  Male,
}

// 对于多余属性
interface PersonInfo {
  name: string,
  age: number,
  gender: Gender,

  // 多余属性1
  // [props: string]: any
}

function logPerson ({ name, age, gender }: PersonInfo): void {
  console.log(`Person：${name},${age} years old,${gender}`)
}

// 类型断言2
// 类型兼容性3
const person = {
  name: 'Samuel',
  age: 25,
  gender: Gender.Male,
  hobby: 'Basketball' // 可以传递多余属性
}
logPerson(person)

// 只读属性
interface ArrInterface {
  0: number, // 0位置只能是number
  readonly 1: string // 1位置只读，不可改
}

const arr:ArrInterface = [123, 'hahah']
// arr[1] = 'good' // Cannot assign to '1' because it is a read-only property.

// 属性名如果是number类型，会自动转换成string
interface RoleDict {
  [id:string]:number
}

const role:RoleDict = {
  12: 123
  // '12':234  // 报 属性重复 的错误
}
console.log(role['12'])

// 接口继承
interface Tomato extends VegetableInterface{
  radius: number
}

// 计数器函数接口
interface CounterInterface {
  (): void, // 函数类型属性, 参数为空，无返回值
  counter: number
}

const countFn = ():CounterInterface => {
  const fn = function () {
    fn.counter++
  }
  fn.counter = 0
  return fn
}

const countFunc:CounterInterface = countFn()
countFunc()
console.log(countFunc.counter)
countFunc()
console.log(countFunc.counter)
