let bool = true
bool = false

// bool = 123  //会报错
console.log(bool)

const myStr = `bool:${bool}`
console.log(myStr)

const nums: number[] = [1, 2, 3, 4, 5]
// nums.push('a')  // 类型不同会报错
console.log(nums)

// 元组
const x: [string, number, boolean] = ['a', 1, false] // 长度和元素类型顺序都有要求
console.log(x)

// 枚举
enum Users {
  CREATOR,
  ADMIN,
  USER
}

console.log(Users.CREATOR)
console.log(Users[2])

const consoleText = (txt: string): void => {
  console.log(txt)
  // 因为没有返回值，声明返回类型为void
}
const res = consoleText('hello')
console.log(res)

/*
const neverVal = (): never => {
  while(true){}
}
*/
const neverVal = (): never => {
  throw new Error('abc')
}

// myStr = neverVal()  // never类型可以赋值给任意类型，但是反过来不可以
function printObj (obj: Record<string, any>): void {
  console.log(obj)
}

printObj({
  name: 'Samuel'
})

// 类型断言
const getLength = (target: (string | number)): number => {
  if ((<string>target).length || (target as string).length === 0) {
    return (<string>target).length
  } else {
    return target.toString().length
  }
}

getLength(123)
