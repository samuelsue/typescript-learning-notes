// 声明合并

// 合并接口
interface InfoInter {
  name:string
  getRes(input:string):number
}
interface InfoInter {
  age:number
  getRes(input:number):string // 同名函数不同类型合并时视为重载
}
const ifo:InfoInter = {
  name: 'Samuel',
  age: 12,
  getRes (input: any): any { // 函数重载的写法,类型保护
    if (typeof input === 'number') return String(input)
    else return input.length
  }
}

// 命名空间和类的合并
// 先定义类
class Validations {
  constructor () {
    console.log('hh')
  }

  public isNumber () {
    console.log('gg')
  }
}
// eslint-disable-next-line
namespace Validations{
  export const numberReg = /^\d+$/
}

console.dir(Validations) // 命名空间的导出变量合并后成为静态属性
console.log(Validations.numberReg)

// 命名空间和函数合并
// 必须先定义函数
/* function counter () {
  counter.n++
}
// eslint-disable-next-line
namespace counter{
  export const n = 0
} */

// 枚举和命名空间合并
enum Colors {
  red,
  green,
  blue
}
// eslint-disable-next-line
namespace Colors {
  export const yellow = 3
}
