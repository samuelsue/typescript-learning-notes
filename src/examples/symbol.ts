// 没有参数的情况
const s1 = Symbol()
const s2 = Symbol()

// console.log(s1 === s2) // false

// // 有参数的情况
// let s1 = Symbol('foo');
// let s2 = Symbol('foo');
//
// s1 === s2 // false
console.log(s1.toString())

const nameSymbol = Symbol('name')
const obj:any = {
  [nameSymbol]: 'Samuel',
  age: 18
}
for (const key in obj) {
  console.log(key)
}
console.log(Object.keys(obj))
console.log(Object.getOwnPropertyNames(obj))

console.log(Object.getOwnPropertySymbols(obj))

class MyclassZ {
  [Symbol.hasInstance] (foo:any) {
    console.log('symbol:', foo)
    return true
  }
}

console.log('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
console.log([1, 2, 3] instanceof MyclassZ)
