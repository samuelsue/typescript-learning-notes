const f = 400

enum Code {
  OK,
  Fault = f, // 可以用const或者函数返回值来编号
  Error = 3, // 紧接着的一个枚举值要手动编号
  Success
}

console.log(Code[400]) // 可以通过编号获取枚举值

// 字符串枚举
enum reply {
  Error = 'Sorry, an error has occurred',
  Success = 'No problem',
  Failed = Error, // 可以引用枚举
}

// 异构枚举, 枚举值类型不同(不推荐)
enum XX {
  a = 1,
  b = 'hehe',
}

// 联合枚举
enum Status {
  on,
  off
}

interface Light {
  status: Status // 只能是Status中的枚举值
}

const light: Light = {
  status: Status.off
}

// const enum
const enum Animal {
  Dog,
  Cat,
  GoldFish
}

const kitty = Animal.Cat // 编译后是kitty=1, 不会产生Animal对象

enum ShapeKind {
  Circle,
  Square
}

interface Circle {
  kind: ShapeKind.Circle,
  radius: number
}

const aC: Circle = {
  kind: ShapeKind.Circle, radius: 0
}
