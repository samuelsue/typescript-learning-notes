// 类型兼容
let fn1 = (x: string, y: string): void => {
  console.log(11)
}
const fn12 = (x: string): void => {
  console.log(22)
}

// fn12 = fn1
fn1 = fn12
fn1('123', '456')

interface Aitf {
  name: string
}

interface Bitf {
  name: string,
  age: number
}

let aObj: Aitf = { name: 'Samuel' }
const bObj: Bitf = { name: 'Samuel', age: 25 }
// bObj = aObj
aObj = bObj

// 重载类型兼容
function shk(x: number, y: number): number
function shk(x: string, y: string): string
function shk (x: any, y: any): any {
  return x + y
}

function shl(x: number, y: number): number
function shl (x: any, y: any): any {
  return x + y
}

let fna = shl
fna = shk // 重载类型少的兼容多的

// 类兼容性
class AC {
  // public static num = 3.14  // 静态成员不参与兼容性检测
  protected num:number
  private age:number
  constructor (public name:string, age:number) {
    this.num = 13
    this.age = age
  }
}

class ACsub extends AC {
  constructor (public name:string, age:number) {
    super(name, age)
    this.num = 15
  }
}

class BC {
  constructor (public name:string) {
    this.name = name
  }
}

let ax:BC = new BC('name') // 结构成员
ax = new AC('hh', 17) // 少的兼容多的
ax = new ACsub('hh', 25) // protected以及private成员必须保证来自同一个类

console.log(ax)
