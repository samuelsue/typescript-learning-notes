// 解构
/*
这里要这么理解，类似函数的默认参数:
fnT(param={a:''}), 就是说这个函数的参数param对象默认是对象包含a属性，a属性如果不存在默认赋值为'' ，因此可以使用fnt()来调用
fnT({a,b}={a:''}), '='前面的是解构,后面的是参数的默认值，就是说如果调用函数时传递了参数(对象)，那么参数里面必须有有a属性,可选b属性
fnT({a,b=123}={a:''}), 表示解构的时候也可以指定默认值b=123
 */
function fnT ({ a, b = 123 } = { a: 'hell' }) :void {
  console.log(a)
  console.log(b)
}

fnT()
fnT({
  a: 'sam',
  b: 123
})
fnT({ a: 'hel' })
// fnT({ b: 567 })

interface funcTypeInterface{
  (start:number):string,
  count: number
}

// 这里这么理解: Greeter是一个变量(对象)，不是说一个具体的类，而是说一个变量(对象)，类似于var Greeter = 构造函数,typeof Greeter就是获取这个Greeter这个对象的类型
class Greeter {
  static greetmsg = 'hey there'
  private readonly greetting:string
  constructor (message?:string) {
    this.greetting = message || ''
  }

  greet () {
    if (this.greetting) {
      return 'hey' + this.greetting
    } else {
      return Greeter.greetmsg
    }
  }
}
type sGreet = typeof Greeter
const greeterMaker: typeof Greeter = Greeter
greeterMaker.greetmsg = 'nonono'
